'use strict';

import React, { Component } from 'react';
import { BackAndroid, Platform, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash/core';
import { popRoute } from './actions/route';
import Navigator from 'Navigator';

import Login from './components/login/';
import SignUp from './components/signup';
import SplashPage from './components/splashscreen/';
import Home from './components/home/';
import Home0 from './components/home0/';
import AddNewDish from './components/addnewdish/';
import AddNewDish0 from './components/addnewdish0/';
import Presentation from './components/presentation/';
import Presentation0 from './components/presentation0/';
import Category from './components/categories/';
import Category0 from './components/categories0/';
import Favourite from './components/favourite/';
import Favourite0 from './components/favourite0/';
import ChooseCategory from './components/chooseCategory/';
import ChooseCategory0 from './components/chooseCategory0/';
import ChooseAddress from './components/chooseAddress/';
import ChooseAddress0 from './components/chooseAddress0/';
import ChooseRestaurant from './components/chooseRestaurant/';
import ChooseRestaurant0 from './components/chooseRestaurant0/';
import InnerCategory from './components/innerCategory/';
import InnerCategory0 from './components/innerCategory0/';
import Review from './components/review/';
import Review0 from './components/review0/';
import ViewMoreReview from './components/viewMoreReview/';
import ViewMoreReview0 from './components/viewMoreReview0/';
import ForgotPassword from './components/forgotpassword/'
import { statusBarColor } from './themes/base-theme';


Navigator.prototype.replaceWithAnimation = function (route) {
    const activeLength = this.state.presentedIndex + 1;
    const activeStack = this.state.routeStack.slice(0, activeLength);
    const activeAnimationConfigStack = this.state.sceneConfigStack.slice(0, activeLength);
    const nextStack = activeStack.concat([route]);
    const destIndex = nextStack.length - 1;
    const nextSceneConfig = this.props.configureScene(route, nextStack);
    const nextAnimationConfigStack = activeAnimationConfigStack.concat([nextSceneConfig]);

    const replacedStack = activeStack.slice(0, activeLength - 1).concat([route]);
    this._emitWillFocus(nextStack[destIndex]);
    this.setState({
        routeStack: nextStack,
        sceneConfigStack: nextAnimationConfigStack,
    }, () => {
        this._enableScene(destIndex);
        this._transitionTo(destIndex, nextSceneConfig.defaultTransitionVelocity, null, () => {
            this.immediatelyResetRouteStack(replacedStack);
        });
    });
};
import Global from './Global';
export var globalNav = {};

const searchResultRegexp = /^search\/(.*)$/;

const reducerCreate = params=>{
    const defaultReducer = Reducer(params);
    return (state, action)=>{
        var currentState = state;

        if(currentState){
            while (currentState.children){
                currentState = currentState.children[currentState.index]
            }
        }
        return defaultReducer(state, action);
    }
};

class AppNavigator extends Component {
    constructor(props){
        super(props);
        console.disableYellowBox = true;

    }
    componentWillMount() {
        
    }
    componentWillUnmount() {
       
    }
    componentDidMount() {
        globalNav.navigator = this._navigator;

        BackAndroid.addEventListener('hardwareBackPress', () => {
            var routes = this._navigator.getCurrentRoutes();

            if(routes[routes.length - 1].id == 'home' || routes[routes.length - 1].id == 'login') {
                return false;
            }
            else {
                this.popRoute();
                return true;
            }
        });
    }

    popRoute() {
        this.props.popRoute();
    }

    render() {
        return (
            <Navigator
                ref={(ref) => this._navigator = ref}
                configureScene={(route) => {
                    return Navigator.SceneConfigs.FadeAndroid;
                }}
                initialRoute={{id: 'splashscreen'}}
                // initialRoute={{id: (Platform.OS === 'android') ? 'splashscreen' : 'login', statusBarHidden: true}}
                renderScene={this.renderScene}
              />
        );
    }

    renderScene(route, navigator) {
        switch (route.id) {
            case 'splashscreen':
                return <SplashPage navigator={navigator} {...route.passProps}/>;
            case 'login':
                return <Login navigator={navigator} {...route.passProps}/>;
            case 'forgotpw':
                return <ForgotPassword navigator={navigator} {...route.passProps}/>;
            case 'signup':
                return <SignUp navigator={navigator} {...route.passProps}/>;
            case 'home':
                return <Home navigator={navigator} {...route.passProps}/>;
            case 'home0':
                return <Home0 navigator={navigator} {...route.passProps}/>;
            case 'addnewdish':
                return <AddNewDish navigator={navigator} {...route.passProps}/>;
            case 'addnewdish0':
                return <AddNewDish0 navigator={navigator} {...route.passProps}/>;
            case 'presentation':
                return <Presentation navigator={navigator} {...route.passProps}/>;
            case 'presentation0':
                return <Presentation0 navigator={navigator} {...route.passProps}/>;
            case 'categories':
                return <Category navigator={navigator} {...route.passProps}/>;
            case 'categories0':
                return <Category0 navigator={navigator} {...route.passProps}/>;
            case 'innerCategory':
                return <InnerCategory navigator={navigator} {...route.passProps} />;
            case 'innerCategory0':
                return <InnerCategory0 navigator={navigator} {...route.passProps} />;
            case 'favourite':
                return <Favourite navigator={navigator} {...route.passProps}/>;
            case 'favourite0':
                return <Favourite0 navigator={navigator} {...route.passProps}/>;
            case 'chooseAddress':
                return <ChooseAddress navigator={navigator} {...route.passProps}/>;
            case 'chooseAddress0':
                return <ChooseAddress0 navigator={navigator} {...route.passProps}/>;
            case 'chooseCategory':
                return <ChooseCategory navigator={navigator} {...route.passProps}/>;
            case 'chooseCategory0':
                return <ChooseCategory0 navigator={navigator} {...route.passProps}/>;
            case 'chooseRestaurant':
                return <ChooseRestaurant navigator={navigator} {...route.passProps}/>;
            case 'chooseRestaurant0':
                return <ChooseRestaurant0 navigator={navigator} {...route.passProps}/>;
            case 'review':
                return <Review navigator={navigator} {...route.passProps}/>;
            case 'review0':
                return <Review0 navigator={navigator} {...route.passProps}/>;
            case 'viewMoreReview':
                return <ViewMoreReview navigator={navigator} {...route.passProps}/>;
            case 'viewMoreReview0':
                return <ViewMoreReview0 navigator={navigator} {...route.passProps}/>;
            default :
                return <Login navigator={navigator}  {...route.passProps}/>;
        }
    }
}
function bindAction(dispatch) {
    return {
        popRoute: () => dispatch(popRoute())
    }
}
const mapStateToProps = (state) => {
    return {
    }
}
export default connect(mapStateToProps, bindAction) (AppNavigator);
