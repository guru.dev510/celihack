'use strict';

var Global = {
    //server
    // SERVER_URL: 'http://ec2-54-169-108-109.ap-southeast-1.compute.amazonaws.com/api/',
    SERVER_URL: 'http://ec2-52-57-131-113.eu-central-1.compute.amazonaws.com/api/',
    //login data
    user_token: null,
    user_id: 0,
    isGuest: false,
    dish_id: 0,
    isReload: false,
    lang: 'En',
    current_lat: 0,
    current_lng: 0,
    categoryImage: [
        require("../category/0.jpg"),
        require("../category/1.jpg"),
        require("../category/2.jpg"),
        require("../category/3.jpg"),
        require("../category/4.jpg"),
        require("../category/5.jpg"),
        require("../category/6.jpg"),
        require("../category/7.jpg"),
        require("../category/8.jpg"),
        require("../category/9.jpg"),
        require("../category/10.jpg"),
        require("../category/11.jpg"),
        require("../category/12.jpg"),
        require("../category/13.jpg"),
        require("../category/14.jpg"),
        require("../category/15.jpg"),
        require("../category/16.jpg"),
        require("../category/17.jpg"),
        require("../category/18.jpg"),
        require("../category/19.jpg"),
    ],

};


export default Global;
