'use strict';

import React, { Component } from 'react';
import { Image, ScrollView, Platform , TouchableOpacity, Text} from 'react-native';
import { connect } from 'react-redux';

import { popRoute ,replaceOrPushRoute} from '../../actions/route';

import { Container, Content, Header, Footer, View, Button, Icon, Card, CardItem } from 'native-base';
import MultipleChoice from 'react-native-multiple-choice'
import theme from '../../themes/base-theme';
import styles from './styles';
import {globalAddNewDish} from '../addnewdish/index.js';
import Autocomplete from 'react-native-autocomplete-input';
import Global from '../../Global';
var {GooglePlacesAutocomplete} = require('react-native-google-places-autocomplete');


class ChooseAddress extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Address: null,
        }
    }
    
    okButtonClick() {
        if (this.state.Address !== null ) {
            globalAddNewDish.addNewDish.changeAddress(this.state.Address.description, this.state.Address.geometry.location.lat, this.state.Address.geometry.location.lng);
        } else {
            globalAddNewDish.addNewDish.changeAddress(this._googlePlace.state.text, Global.current_lat, Global.current_lng);
        }
        this.popRoute();
    }
    popRoute() {
        this.props.popRoute();
    }
    navigateTo(route) {
        this.props.replaceOrPushRoute(route);
    }
    addSelectedCategory(option) {
        for(var i=0; i < 3;i++){
            if(option === selectedOption[i]){
                selectedOption.splice(i, 1);
                return;
            }
        }
        selectedOption.push(option);
        if(selectedOption.length === 4)
            selectedOption.splice(0, 1);
    }
    showSelected(){
      categoryIds = "";
      for(var i=0; i < categories.length; i++){
        for(var j=0; j < selectedOption.length; j++){
          if(categories[i] === selectedOption[j])
            categoryIds = categoryIds + (i + 1)+ ","
        }
      }
    }

    render() {
        
        return (
            <Container theme={theme} style={{backgroundColor: theme.defaultBackgroundColor}}>
                <Header style={{ height: theme.headerHeight, paddingTop: (Platform.OS==='ios') ? 23 : 9}}>
                    <View style = {{flex:1, flexDirection:'row',justifyContent: 'space-between', }}>
                        <Button transparent onPress={() => this.popRoute()}>
                            <Icon name='ios-arrow-back' style={styles.titleArrow} />
                            Choose Address
                        </Button>
                        <Button transparent onPress={() => this.okButtonClick()}>
                            OK
                        </Button>
                    </View>
                </Header>
                <View style={{backgroundColor: 'transparent', padding:10, margin: 10}} >
                    <GooglePlacesAutocomplete
                        ref={(ref) => this._googlePlace = ref}
                        placeholder='Search'
                        minLength={1} 
                        autoFocus={false}
                        fetchDetails={true}
                        onPress={(data, details = null) => {
                            var selectAddress = {description: data.description, geometry: { location: { lat: details.geometry.location.lat, lng: details.geometry.location.lng } }};
                            this.setState({
                                Address: selectAddress
                            })
                        }}
                        getDefaultValue={() => {
                            return ''; // text input default value
                        }}
                        query={{
                            // available options: https://developers.google.com/places/web-service/autocomplete
                            // key: 'AIzaSyCN6_e9HkjImfiEZrdwQMQ9RWf63_cRsu8',
                            key: 'AIzaSyCarBVhApvDXc7Tz2Wh2SC2oFdrMoLZ7gE',
                            language: 'en', // language of the results
                            types: 'address', // default: 'geocode'
                        }}
                        styles={{
                            description: {
                                fontWeight: 'bold',
                            },
                            predefinedPlacesDescription: {
                                color: '#1faadb',
                            },
                        }}

                        currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
                        currentLocationLabel="Current location"   ///GoogleReverseGeocoding  GooglePlacesSearch
                        nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                        GoogleReverseGeocodingQuery={{
                        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro

                        }}
                        latitude={ Global.current_lat}
                        longitude={ Global.current_lng}
                        GooglePlacesSearchQuery={{
                        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search

                        }}
                        enablePoweredByContainer = {true}

                        filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

                    
                    />
                </View>
            </Container>
        )
    }
}

function bindAction(dispatch) {
    return {
        popRoute: () => dispatch(popRoute()),
        replaceOrPushRoute:(route)=>dispatch(replaceOrPushRoute(route))
    }
}

export default connect(null, bindAction)(ChooseAddress);
