'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Platform, TouchableOpacity, Image, Text, Linking, Alert} from 'react-native';
import { Container, Header, Content, Footer, Icon} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import { Grid, Col, Row } from "react-native-easy-grid";

import { popRoute, pushNewRoute, replaceRoute } from '../../actions/route';
import {globalHome} from '../home/index.js';
import theme from '../../themes/base-theme';
import styles from './styles';
import Global from '../../Global';
import Util from '../../utils.js';
import Share, {ShareSheet, Button} from 'react-native-share';
var reviewList = [
    {ReviewOwner: '', Review: '', created_at:''},
    {ReviewOwner: '', Review: '', created_at:''}
];
export var globalPresentation = {};

class Presentation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dish: [],
            review: null,
            isLoading: false,
            visible: false,
        }
    }
    componentWillMount() {
        this.loadDishData();
        globalPresentation.handle = this;

    }
    loadDishData() {
        this.setState({
            isLoading: true,
        })
        fetch( Global.SERVER_URL + 'dish/' + Global.dish_id, {
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            method: "GET"
        })
        .then((response) => response.json())
        .then((responseData) => {
            if(responseData.length === 0 ) {
                this.setState({
                    isLoading: false,
                });
            }else {
                if (responseData.status_code !== 500) {
                    this.handleDetailResponse(responseData);
                }
            }
        })
        .done();        
    }   
    handleDetailResponse(responseData) {
        this.setState({
            isLoading: false,
            dish: responseData.dish,
            review: responseData.dish.review 
        });
    }
    navigateTo(route) {
        this.props.pushNewRoute(route);
    }
    gotoWriteReview() {
        if (Global.isGuest === true) {
            Alert.alert(
                '',
                'Sorry, you need to register in order to add a new dish!',
                [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed!')},
                    {text: 'Register', onPress: () => this.props.replaceRoute('signup')},
                ]
            );  
        } else {
            this.props.navigator.push({
                id: 'review',
                passProps: {
                    effectCount : this.state.dish.SideEffectGood + this.state.dish.SideEffectBad,
                }
            });
        }
        
    }
    
    popRoute() {
        if (Global.isReload === true) {
            Global.isReload = false;
            globalHome.handle.loadDishData();
        }
        this.props.popRoute();
    }
    gotoViewReviewAll() {
        this.navigateTo('viewMoreReview');
    }
    gotoFavorite() {
        if (Global.isGuest === true) {
            Alert.alert(
                '',
                'Sorry, you need to register in order to add a new dish!',
                [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed!')},
                    {text: 'Register', onPress: () => this.props.replaceRoute('signup')},
                ]
            );  
        } else {
            this.navigateTo('favourite');
        }
    }
    gotoAddNewDish() {
        if (Global.isGuest === true) {
            Alert.alert(
                '',
                'Sorry, you need to register in order to add a new dish!',
                [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed!')},
                    {text: 'Register', onPress: () => this.props.replaceRoute('signup')},
                ]
            );  
        } else {
            this.navigateTo('addnewdish');
        }
    }
    onCancel() {
        this.setState({visible:false});
    }
    onOpen() {
        this.setState({visible:true});
    }
    render() {
        if(this.state.review != null) {
            if (this.state.review.length == 0) {
                reviewList[0].ReviewOwner = '';
                reviewList[0].Review = '';
                reviewList[0].created_at = '';
                reviewList[1].ReviewOwner = '';
                reviewList[1].Review = '';
                reviewList[1].created_at = ''
            }else if(this.state.review.length == 1){
                reviewList[0] = this.state.review[0];
                reviewList[1].ReviewOwner = '';
                reviewList[1].Review = '';
                reviewList[1].created_at = '';
            } else {
                reviewList = this.state.review
            }
        }
        let distance;
        if (Global.current_lat === 0) {
            distance = 'calculating...'
        }else {
            distance = ''+ Math.round(Util.getDistanceFromLatLonInKm(Global.current_lat,Global.current_lng,this.state.dish.Latitude,this.state.dish.Longitude))+' km away';
        }
        let UserAndDateOne;
        if (reviewList[0].ReviewOwner ==='') {
            UserAndDateOne = '';
        } else {
            var date = reviewList[0].created_at.split(' ', 1);
            UserAndDateOne = '('+reviewList[0].ReviewOwner + ', '+date+')';
        }
        let UserAndDateTwo;
        if (reviewList[1].ReviewOwner ==='') {
            UserAndDateTwo = '';
        } else {
            var date = reviewList[1].created_at.split(' ', 1);
            UserAndDateTwo = '('+reviewList[1].ReviewOwner + ', '+date+')';
        }
        return (

            <Container theme={theme} style={{backgroundColor: '#ffffff'}}>
                <Header style={{justifyContent: 'flex-start', paddingTop: (Platform.OS==='ios') ? 23 : 9, height: theme.headerHeight}}>
                    <Grid>
                        <Col style={styles._col1}>
                            <TouchableOpacity transparent onPress={() => this.popRoute()}>
                                <Icon name='ios-arrow-back' style={styles.titleArrow} />
                            </TouchableOpacity>
                        </Col>
                        <Col style={styles._col2}>
                            <TouchableOpacity style={styles._align} onPress={() => this.navigateTo('categories')}>
                                <Image style={styles._toolbarImage} resizeMode={'stretch'} source={require('../../../images/food_menu_icon.png')} />
                                <Text style={styles._toolbarText}>Food Categories</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={styles._col2}>
                            <TouchableOpacity style={styles._align} onPress={() => this.gotoFavorite()}>
                                <Image style={styles._toolbarImage} resizeMode={'stretch'} source={require('../../../images/ratings.png')} />
                                <Text style={styles._toolbarText}>Favorites</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={styles._col2}>
                            <TouchableOpacity style={styles._align} onPress={() => this.gotoAddNewDish()}>
                                <Image style={styles._toolbarImage} resizeMode={'stretch'} source={require('../../../images/plus_icon.png')} />
                                <Text style={styles._toolbarText}>Add a new dish</Text>
                            </TouchableOpacity>
                        </Col>
                    </Grid>
                </Header>
                <Content>
                    <Spinner visible={this.state.isLoading} />
                    <Grid style={{padding:5}}>
                        <Image style={styles._mainImage} resizeMode={'stretch'} source={{uri:this.state.dish.Image}}>
                          <View style={{flex:4, height:theme.width*0.6}}>
                          </View>
                          <Image style={styles._mainTransImage} source={require('../../../images/transparent_bar.png')}>
                              <Text numberOfLines={1} style={{fontSize:20, color:'#fff', marginLeft:15, marginRight:15}}>{ this.state.dish.DishName }</Text>
                          </Image>
                        </Image>
                        <Row style={{flex:1, padding:5, marginTop:10}}>
                            <Col style={{flex:1, marginLeft: 10,}}>
                                <Icon name='ios-pin' style={{fontSize:30,color:'#74cdc1'}}/>
                            </Col>
                            <Col style={{flex:7}}>
                                <Text numberOfLines={1} style={{color:theme.customColor}}>{this.state.dish.RestaurantName}</Text>
                            </Col>
                            <Col style={{flex:4}}>
                                <TouchableOpacity onPress={() =>this.onOpen()}>
                                    <Text numberOfLines={1} style={{color:theme.iconColor}}>{distance}</Text>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                        <Row style={{flex:1, marginTop:5}}>
                            <Col style={{flex:1}}>
                            </Col>
                            <Col style={{flex:10, marginLeft:5}}>
                                <Text numberOfLines={1} style={{color:theme.customColor}}>{this.state.dish.Address}</Text>
                            </Col>
                        </Row>
                        
                        <Row style={styles._rowPrice}>
                            <Col style={{flex:1}}>
                                <Image style={styles._priceImage} resizeMode={'stretch'} source={require('../../../images/price.png')}/>
                            </Col>
                            <Col style={{flex:14}}>
                                <Text style={{marginLeft:10, color:theme.customColor}}>{this.state.dish.Price}</Text>
                            </Col>
                            
                        </Row>
                        <Row style={{flex:1, padding:5}}>
                            <Col style={{flex:1}}/>
                            <Col style={{flex:5}}>
                                <Text style={{color:theme.customColor}}>Celiac side effects</Text>
                            </Col>
                            <Col style={{flex:2}}>
                                <Image style={styles._emojiImage1} source={require('../../../images/emoji1.png')}/>
                            </Col>
                            <Col style={{flex:2}}>
                                <Image style={styles._emojiImage2} source={require('../../../images/emoji2.png')}/>
                            </Col>
                            <Col style={{flex:1}}/>
                        </Row>
                        <Row style={{flex:1, padding:5}}>
                            <Col style={{flex:1}}/>
                            <Col style={{flex:5}}/>
                            <Col style={{flex:2}}>
                                <Text style={{color:theme.customColor, marginLeft:15}}>{this.state.dish.SideEffectGood}</Text>
                            </Col>
                            <Col style={{flex:2}}>
                                <Text style={{color:theme.customColor, marginLeft:10}}>{this.state.dish.SideEffectBad}</Text>
                            </Col>
                            <Col style={{flex:1}}/>
                        </Row>
                        <Row style={styles._rowUser}>
                            <Col style={{flex:1}}>
                              <Image style={styles._userProfile} resizeMode={'stretch'} source={require('../../../images/user.png')}/>
                            </Col>
                            <Col style={{flex:12}}>
                              <Text style={{marginLeft:10, color:theme.customColor}}>{this.state.dish.UserName}</Text>
                            </Col>
                        </Row>
                        <Row style={styles._rowReview}>
                            <Col style={{flex:1}}>
                                <Icon name='ios-chatbubbles' style={{fontSize:23}}/>
                            </Col>
                            <Col style={{flex:6}} />
                            <Col style={{flex:5}}>
                                <TouchableOpacity  onPress={() => this.gotoWriteReview()}><Text style={styles._textReview1} > + Add a Review</Text></TouchableOpacity>    
                            </Col>
                        </Row>
                        <View style={{flexDirection: 'column', marginTop:10, marginLeft:10, marginRight:10, justifyContent:'center'}}>
                            <Text style={{color:theme.customColor}}>{reviewList[0].Review}</Text>
                            <Text style={{ color:theme.customColor, }}>{UserAndDateOne}</Text>
                        </View>
                        <View style={{flexDirection: 'column', marginTop:10, marginLeft:10, marginRight:10,justifyContent:'center'}}>
                            <Text style={{color:theme.customColor}}>{reviewList[1].Review}</Text>
                            <Text style={{color:theme.customColor}}>{UserAndDateTwo}</Text>
                        </View>
                        <ShareSheet visible={this.state.visible} onCancel={this.onCancel.bind(this)}>
                            <Button iconSrc={require('../../../images/waze.png')}
                                    onPress={()=>{
                                this.onCancel();
                                setTimeout(() => {
                                    const Url = 'waze://?ll='+this.state.dish.Latitude+','+this.state.dish.Longitude+'&navigate=yes';
                                    Linking.openURL(Url).catch(err => 
                                        Alert.alert(
                                            '',
                                            'You have to install Waze app!',
                                            [
                                                {text: 'OK', onPress: () => console.log('Cancel Pressed!')},
                                            ]
                                        ));
                                },300);
                                }}>Waze</Button>
                            <Button iconSrc={require('../../../images/map.png')}
                                    onPress={()=>{
                                this.onCancel();
                                setTimeout(() => {
                                    if (Platform.OS === 'android') {
                                        const newUrl = 'http://maps.google.com?saddr='+Global.current_lat+','+Global.current_lng+'&daddr=' + this.state.dish.Latitude + ',' + this.state.dish.Longitude;
                                        Linking.openURL(newUrl);
                                    } else {
                                        const newUrl = 'https://maps.apple.com?saddr='+Global.current_lat+','+Global.current_lng+'&daddr=' + this.state.dish.Latitude + ',' + this.state.dish.Longitude;
                                        Linking.openURL(newUrl);
                                    }
                                    
                                },300);
                                }}>Maps</Button>
                        </ShareSheet>
                    </Grid>
                    
                </Content>
                <Footer>
                  <TouchableOpacity onPress={() => this.gotoViewReviewAll()}>
                      <Text style={{color:theme.iconColor}}>All reviews</Text>
                  </TouchableOpacity>
                </Footer>
            </Container>
        )
    }
}

function bindAction(dispatch) {
    return {
        popRoute: () => dispatch(popRoute()),
        pushNewRoute:(route)=>dispatch(pushNewRoute(route)),
        replaceRoute:(route)=>dispatch(replaceRoute(route))
    }
}

export default connect(null, bindAction)(Presentation);
