'use strict';

import React, { Component } from 'react';
import { Image, Platform, TouchableOpacity, AsyncStorage} from 'react-native';
import { connect } from 'react-redux';
import Global from '../../Global';
import { USER_ID, LANGUAGE } from '../../Constants';

var navigator;

export default class SplashPage extends Component {
    componentWillMount () {
        navigator = this.props.navigator;
        AsyncStorage.getItem(USER_ID)
        .then( (value) => { 
            if (value != null) {
                Global.user_id = parseInt(value);
                AsyncStorage.getItem(LANGUAGE)
                .then( (value) => { 
                    Global.lang = value;
                    if (Global.lang === 'En') {
                        navigator.replace({ id: 'home' });
                    } else {
                        navigator.replace({ id: 'home0' });
                    }
                });
            } else {
                navigator.replace({ id: 'login' });
            }
        });
        
    }
    
    componentDidMount() {
        this.gotoMainPage();
    }
    
    gotoMainPage() {
        if (Global.user_id == 0) {
            
        } else {
            
        }
    }
    render () {
        return (
            <Image source={require('../../../images/launchscreen.png')} style={{flex: 1, height: null, width: null}} />
        );
    }
}
